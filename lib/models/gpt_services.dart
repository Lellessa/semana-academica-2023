import 'dart:convert';
import 'package:http/http.dart' as http;

class GPTServices {
  static Future<Map<String, dynamic>> doRequisition(String prompt) async {
    String url = 'https://api.openai.com/v1/completions';

    String apiKey = 'SUA CHAVE OPEN AI API AQUI';
    String orgId = 'SUA ORGANIZATION ID AQUI';
    var header = {
      'Authorization': 'Bearer $apiKey',
      'OpenAI-Organization': orgId,
      'Content-Type': 'application/json;charset=utf-8',
      'Accept': 'application/json;charset=utf-8'
    };

    Map<String, dynamic> body = {
      'model': 'gpt-3.5-turbo-instruct',
      'prompt': prompt,
      'max_tokens': 3900,
      'temperature': 0
    };

    http.Response response = await http.post(Uri.parse(url),
        headers: header, body: json.encode(body));

    if (response.statusCode == 200) {
      return decodeToMap(response.body);
    } else {
      print('ERROR: ${response.statusCode}');
      return {'status': response.statusCode};
    }
  }

  static Map<String, dynamic> decodeToMap(String body) {
    Map<String, dynamic> bodyMap = json.decode(utf8.decode(body.codeUnits));

    String trainingJson = bodyMap['choices'][0]['text'];
    trainingJson = trainingJson.replaceAll('\\n', '').replaceAll('\\', '');

    Map<String, dynamic> trainingMap = json.decode(trainingJson);
    return trainingMap;
  }
}
