import 'package:flutter/material.dart';

class MainToggleButtons extends StatefulWidget {
  const MainToggleButtons({
    required this.inputs,
    super.key,
  });

  final Map<String, dynamic> inputs;

  @override
  State<MainToggleButtons> createState() => _MainToggleButtonsState();
}

class _MainToggleButtonsState extends State<MainToggleButtons> {
  List<bool> condicionamentoSelected = [true, false, false];
  List<String> condicionamentos = ['iniciante', 'intermediário', 'avançado'];

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      alignment: Alignment.center,
      child: ToggleButtons(
        fillColor: Theme.of(context).colorScheme.primary,
        selectedColor: Colors.white,
        selectedBorderColor: Theme.of(context).colorScheme.primary,
        borderRadius: BorderRadius.circular(20),
        constraints: BoxConstraints(
            minHeight: 50, minWidth: MediaQuery.of(context).size.width * 0.28),
        isSelected: condicionamentoSelected,
        onPressed: (int index) {
          setState(() {
            widget.inputs['condicionamento'] = condicionamentos[index];
            for (int i = 0; i < 3; i++) {
              condicionamentoSelected[i] = i == index;
            }
          });
        },
        children: const [
          Text('Iniciante'),
          Text('Intermediário'),
          Text('Avançado')
        ],
      ),
    );
  }
}
