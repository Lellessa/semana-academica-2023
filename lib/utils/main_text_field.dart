import 'package:flutter/material.dart';

class MainTextField extends StatelessWidget {
  const MainTextField({
    super.key,
    required this.inputs,
  });

  final Map<String, dynamic> inputs;

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: InputDecoration(
          hintText: 'Ganho de massa muscular, perda de peso..',
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20))),
      onChanged: (String text) {
        if (text != '') {
          inputs['objetivo'] = text;
        }
      },
    );
  }
}
