import 'package:flutter/material.dart';

class MainButton extends StatelessWidget {
  const MainButton({required this.onPressed, required this.text, super.key});

  final String text;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPressed,
      style: ButtonStyle(
          padding: const MaterialStatePropertyAll(
              EdgeInsets.symmetric(horizontal: 60, vertical: 15)),
          backgroundColor:
              MaterialStatePropertyAll(Theme.of(context).colorScheme.primary)),
      child: Text(
        text,
        style: const TextStyle(color: Colors.white),
      ),
    );
  }
}
