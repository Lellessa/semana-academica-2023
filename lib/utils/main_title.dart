import 'package:flutter/material.dart';

class MainTitle extends StatelessWidget {
  const MainTitle({
    required this.text,
    super.key,
  });
  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(bottom: 10, top: 25),
        child: Text(text, style: const TextStyle(fontSize: 16)));
  }
}
