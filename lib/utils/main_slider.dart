import 'package:flutter/material.dart';

class MainSlider extends StatefulWidget {
  const MainSlider({
    super.key,
    required this.inputs,
  });

  final Map<String, dynamic> inputs;

  @override
  State<MainSlider> createState() => _MainSliderState();
}

class _MainSliderState extends State<MainSlider> {
  int selectedTime = 5;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(Icons.alarm, color: Theme.of(context).colorScheme.primary),
            const SizedBox(width: 10),
            Text(
              '$selectedTime minutos',
              style: const TextStyle(fontSize: 16),
            )
          ],
        ),
        Slider(
            value: selectedTime.toDouble(),
            divisions: 18,
            min: 5,
            max: 185,
            onChanged: (double value) {
              setState(() {
                selectedTime = value.round();
                widget.inputs['tempo'] = selectedTime;
              });
            }),
      ],
    );
  }
}
