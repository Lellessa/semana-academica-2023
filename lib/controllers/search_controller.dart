import 'dart:convert';

import 'package:teste_exercita_ai/models/gpt_services.dart';

class MySearchController {
  Future<Map<String, dynamic>> getTraining(Map<String, dynamic> inputs) async {
    String prompt =
        'Faça uma ficha de treino com objetivo de ${inputs['objetivo']}, para um praticante ${inputs['condicionamento']} sem restrições de saúde, com duração de ${inputs['tempo']} minutos. Retorne utilizando o formato JSON com as informações de nome da ficha de treino, duração total, nome do exercício, séries, repetições e tempo de descanso (segundos).';

    Map<String, dynamic> trainingMap = await GPTServices.doRequisition(prompt);
    print(trainingMap);
    return trainingMap;
  }
}
