import 'package:flutter/material.dart';
import 'package:teste_exercita_ai/controllers/search_controller.dart';
import 'package:teste_exercita_ai/utils/main_button.dart';
import 'package:teste_exercita_ai/utils/main_slider.dart';
import 'package:teste_exercita_ai/utils/main_text_field.dart';
import 'package:teste_exercita_ai/utils/main_title.dart';
import 'package:teste_exercita_ai/utils/main_toggle_buttons.dart';
import 'package:teste_exercita_ai/viewes/training_screen.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({super.key});

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  Map<String, dynamic> inputs = {
    'objetivo': '',
    'condicionamento': '',
    'tempo': 5
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Exercita AI',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 25),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // OBJETIVO
            const MainTitle(
              text: 'Qual o objetivo do treino?',
            ),
            MainTextField(inputs: inputs),

            // CONDICIONAMENTO
            const MainTitle(
              text: 'Nível de condicionamento',
            ),
            MainToggleButtons(inputs: inputs),

            // TEMPO
            const MainTitle(
              text: 'Disponibilidade de tempo',
            ),
            MainSlider(inputs: inputs),

            Expanded(
              child: Container(
                width: MediaQuery.of(context).size.width,
                padding: const EdgeInsets.only(bottom: 60),
                alignment: Alignment.bottomCenter,
                child: MainButton(
                  text: 'Gerar ficha',
                  onPressed: () {
                    MySearchController sc = MySearchController();
                    // sc.getTraining(inputs);
                    sc
                        .getTraining(inputs)
                        .then((Map<String, dynamic> trainingMap) {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) => TrainingScreen(
                                    trainingMap: trainingMap,
                                  )));
                    });
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
