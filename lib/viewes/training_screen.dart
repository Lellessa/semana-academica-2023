import 'package:flutter/material.dart';

class TrainingScreen extends StatelessWidget {
  const TrainingScreen({super.key, required this.trainingMap});

  final Map<String, dynamic> trainingMap;

  @override
  Widget build(BuildContext context) {
    /* Map<String, dynamic> trainingMap = {
      "nome_ficha": "Treino para ganho de massa muscular",
      "duracao_total": "60 minutos",
      "exercicios": [
        {
          "nome_exercicio": "Supino reto",
          "series": 3,
          "repeticoes": 10,
          "tempo_descanso": 60
        },
        {
          "nome_exercicio": "Agachamento livre",
          "series": 3,
          "repeticoes": 12,
          "tempo_descanso": 60
        },
        {
          "nome_exercicio": "Levantamento terra",
          "series": 3,
          "repeticoes": 8,
          "tempo_descanso": 60
        },
        {
          "nome_exercicio": "Desenvolvimento com halteres",
          "series": 3,
          "repeticoes": 12,
          "tempo_descanso": 60
        },
        {
          "nome_exercicio": "Remada curvada",
          "series": 3,
          "repeticoes": 10,
          "tempo_descanso": 60
        },
        {
          "nome_exercicio": "Rosca direta com barra",
          "series": 3,
          "repeticoes": 12,
          "tempo_descanso": 60
        },
        {
          "nome_exercicio": "Tríceps testa",
          "series": 3,
          "repeticoes": 10,
          "tempo_descanso": 60
        },
        {
          "nome_exercicio": "Abdominal infra",
          "series": 3,
          "repeticoes": 15,
          "tempo_descanso": 60
        },
        {
          "nome_exercicio": "Flexão de braço",
          "series": 3,
          "repeticoes": 12,
          "tempo_descanso": 60
        }
      ]
    }; */

    return Scaffold(
      appBar: AppBar(
        title: const Text('Ficha de Treino'),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 25),
        margin: const EdgeInsets.only(top: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // TITLE
            Text(
              trainingMap['nome_ficha'],
              style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 20,
            ),

            // DURATION
            const Text(
              'Duração:',
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
            Text(
              trainingMap['duracao_total'],
              style: const TextStyle(fontSize: 16, color: Colors.black45),
            ),
            const SizedBox(
              height: 20,
            ),

            // EXERCISES
            const Row(
              children: [
                Text(
                  'Exercícios',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child:
                      Divider(height: 3, color: Colors.black87, thickness: 1.5),
                )
              ],
            ),
            const SizedBox(
              height: 20,
            ),

            Expanded(
              child: ListView.builder(
                itemCount: trainingMap['exercicios'].length,
                itemBuilder: (BuildContext context, int index) {
                  return MainExerciseTopic(
                      exerciseTopic: trainingMap['exercicios'][index]);
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}

class MainExerciseTopic extends StatelessWidget {
  const MainExerciseTopic({
    required this.exerciseTopic,
    super.key,
  });

  final Map<String, dynamic> exerciseTopic;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: 12,
            height: 12,
            margin: const EdgeInsets.only(right: 20, top: 5),
            decoration: BoxDecoration(
                color: Colors.black87, borderRadius: BorderRadius.circular(20)),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                exerciseTopic['nome_exercicio'],
                style:
                    const TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
              ),
              const SizedBox(height: 3),
              Text('Séries: ${exerciseTopic['series']}'),
              const SizedBox(height: 3),
              Text('Repetições: ${exerciseTopic['repeticoes']}'),
              const SizedBox(height: 3),
              Text('Descanso: ${exerciseTopic['tempo_descanso']}')
            ],
          )
        ],
      ),
    );
  }
}
